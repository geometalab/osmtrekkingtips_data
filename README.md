# OSMTrekkingTips Data

This repository contains the template and the data that is used by the 
[OSMTrekkingTips](https://gitlab.com/geometalab/osmtrekkingtips) Twitter bot to 
generate tweets about OpenStreetMap.